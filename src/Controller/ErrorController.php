<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/error")
 */
class ErrorController extends AbstractController
{
    /**
     * @Route("/403", name="error_403", methods={"GET"})
     */
    public function error403(){
        return $this->render('error/error403.html.twig');
    }
}
