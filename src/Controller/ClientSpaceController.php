<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/espace-client")
 * @IsGranted("ROLE_CLIENT")
 */
class ClientSpaceController extends AbstractController
{
    /**
     * @Route("/", name="client_space")
     */
    public function index(){
        return $this->render('client_space/index.html.twig', [
            'controller_name' => 'ClientSpaceController',
        ]);
    }
}
